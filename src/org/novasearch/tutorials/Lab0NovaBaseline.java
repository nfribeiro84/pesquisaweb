package org.novasearch.tutorials;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;

public class Lab0NovaBaseline {

	static String indexPath = "data/index";
	static String docPath = "data/Answers.csv";
	static String pageRankPath = "data/pagerank.txt";
	static String queriesPath = "/data/Queries.csv";
	static String labQueries = "data/labQueries.txt";
	static String offlineQueries = "data/queries.offline.txt";
	static String queries = "data/queries.kaggle.txt";
	static String results = "data/results.csv";
	static String evaluationQueries = "data/queries.kaggle-public.txt";
	
	// Whether to present all optional output or not
	static boolean flag_verbose = false;
	
	// Whether to use the evaluation queries or not
	static boolean flag_evaluation = false;
	
	// Ao fazer set desta flag a true, corre orientado para o TREC_EVAL.
	// Para command line:
	// java -Ddev=true -cp bin:lib/* org.novasearch.tutorials.Lab0NovaBaseline
	// Para correr no eclipse:
	// Meter "-Ddev=true" no campo JVM arguments.
	static final String DEV_PROP = System.getProperty("dev");
	static final boolean DEV_MODE = DEV_PROP == null ? false : DEV_PROP.equalsIgnoreCase("true");
	
	boolean create = true;

	private IndexWriter idx;
	// Vector Space Model Similarity, using TFIDF as weights
//	private TFIDFSimilarity sim = new ClassicSimilarity();
	
	// Probabilistic model BM25 Similarity
	private BM25Similarity sim = new BM25Similarity();
	
	// Dirichlet Smoothing
//	private LMDirichletSimilarity sim = new LMDirichletSimilarity();

	void openIndex(Analyzer analyzer) {
		try {
			// ====================================================
			// Select the data analyser to tokenise document data

			// ====================================================
			// Configure the index to be created/opened
			//
			// IndexWriterConfig has many options to be set if needed.
			//
			// Example: for better indexing performance, if you
			// are indexing many documents, increase the RAM
			// buffer. But if you do this, increase the max heap
			// size to the JVM (eg add -Xmx512m or -Xmx1g):
			//
			// iwc.setRAMBufferSizeMB(256.0);
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
			if (create) {
				// Create a new index, removing any
				// previously indexed documents:
				iwc.setOpenMode(OpenMode.CREATE);		
				iwc.setSimilarity(sim);
			} else {
				// Add new documents to an existing index:
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}

			// ====================================================
			// Open/create the index in the specified location
			Directory dir = FSDirectory.open(Paths.get(indexPath));
			idx = new IndexWriter(dir, iwc);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	void indexDocuments() {
		if (idx == null)
			return;

		// ====================================================
		// Parse the Answers data
		try (BufferedReader br = new BufferedReader(new FileReader(docPath))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine(); // The first line is dummy
			line = br.readLine();

			// ====================================================
			// Read documents
			while (line != null) {
				int i = line.length();

				// Search for the end of document delimiter
				if (i != 0)
					sb.append(line);
				sb.append(System.lineSeparator());
				if (((i >= 2) && (line.charAt(i - 1) == '"') && (line.charAt(i - 2) != '"') && (line.charAt(i - 2) != ','))
						|| ((i == 1) && (line.charAt(i - 1) == '"'))) {
					// Index the document
					indexDoc(sb.toString());

					// Start a new document
					sb = new StringBuilder();
				}
				line = br.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void indexDoc(String rawDocument) {

		Document doc = new Document();

		// ====================================================
		// Each document is organized as:
		// Id,OwnerUserId,CreationDate,ParentId,Score,Body
		Integer Id = 0;
		try {

			// Extract field Id
			Integer start = 0;
			Integer end = rawDocument.indexOf(',');
			String aux = rawDocument.substring(start, end);
			Id = Integer.decode(aux);
			doc.add(new IntPoint("AnswerId", Id));
			doc.add(new StoredField("AnswerId",Id));

			// Extract field OwnerUserId
			start = end + 1;
			end = rawDocument.indexOf(',', start);
			aux = rawDocument.substring(start, end);
			Integer OwnerUserId = 0;
			try
			{
				OwnerUserId = Integer.decode(aux);
			}catch(NumberFormatException w)
			{
				if(flag_verbose)
					System.out.println(w);
			}
			doc.add(new IntPoint("OwnerUserId", OwnerUserId));
			doc.add(new StoredField("OwnerUserId", OwnerUserId));

			// Extract field CreationDate
			try {
				start = end + 1;
				end = rawDocument.indexOf(',', start);
				aux = rawDocument.substring(start, end);
				Date creationDate;
				creationDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(aux);
				doc.add(new LongPoint("CreationDate", creationDate.getTime()));
			} catch (ParseException e1) {
				System.out.println("Error parsing date for document " + Id);
			}

			// Extract field ParentId
			start = end + 1;
			end = rawDocument.indexOf(',', start);
			aux = rawDocument.substring(start, end);
			Integer ParentId = Integer.decode(aux);
			doc.add(new IntPoint("Id", ParentId));

			// Extract field Score
			start = end + 1;
			end = rawDocument.indexOf(',', start);
			aux = rawDocument.substring(start, end);
			Integer Score = Integer.decode(aux);
			doc.add(new IntPoint("Score", Score));

			// Extract field Body
			String body = rawDocument.substring(end + 1);
			body = html2text(body);
			doc.add(new TextField("Body", body, Field.Store.YES));
			

		// ====================================================
		// Add the document to the index
			if (idx.getConfig().getOpenMode() == OpenMode.CREATE) {
				if(flag_verbose)
					System.out.println("adding " + Id);
				idx.addDocument(doc);
			} else {
				idx.updateDocument(new Term("Id", Id.toString()), doc);
			}
		} catch (IOException e) {
			System.out.println("Error adding document " + Id);
		} catch (Exception e) {
		System.out.println("Error parsing document " + Id);
		}
	}
	
	public static String html2text(String html) {
	    return Jsoup.parse(html).text();
	}
	
	ArrayList<QueryDoc> readQueries()
	{
		// Read queries file and store it on a list
		try (BufferedReader br = new BufferedReader(new FileReader(queries))) 
		{
			ArrayList<QueryDoc> queries = new ArrayList<QueryDoc>();
			String line = br.readLine();
			
			// ====================================================
			// Read documents
			while (line != null) 
			{
				int separator = line.indexOf(":");
				int queryId = Integer.parseInt(line.substring(0,separator));
				String queryText = line.substring(separator + 1);
				queries.add(new QueryDoc(queryId, queryText));
				if(flag_verbose){
					System.out.println(queryId);
					System.out.println(queryText);
					System.out.println();
				}
				line = br.readLine();
			}
			
			return queries;
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	ArrayList<SearchResult> indexSearch(Analyzer analyzer, String stringQuery, HashMap<Integer, Double> userRank, double alpha) 
	{
		IndexReader reader = null;
		try 
		{
			reader = DirectoryReader.open(FSDirectory.open(Paths.get(indexPath)));
			IndexSearcher searcher = new IndexSearcher(reader);
			searcher.setSimilarity(sim);

			QueryParser parser = new QueryParser("Body", analyzer);
			
			if(stringQuery == null || stringQuery.length() == -1)
				return new ArrayList<SearchResult>();
			
			stringQuery = stringQuery.trim();
			if(stringQuery.length() == 0)
				return new ArrayList<SearchResult>();
			
			Query query;
			try 
			{
				query = parser.parse(QueryParser.escape(stringQuery));
				// The queryparser.escape inserts the escape character on special characters
				//query = parser.parse(stringQuery);
			} 
			catch (org.apache.lucene.queryparser.classic.ParseException e) 
			{
				System.out.println("Error parsing query string.");
				return new ArrayList<SearchResult>();
			}
			
			ArrayList<SearchResult> resultList = new ArrayList<SearchResult>();

			// Get the best 50 results for the query
			TopDocs results = searcher.search(query, 50);
			ScoreDoc[] hits = results.scoreDocs;
			double[] calculatedScore = new double[50];
			
			// Normalizing scores
			float maxScore = 0;
			for(ScoreDoc doc : hits)
				if(doc.score > maxScore)
					maxScore = doc.score;
			
			for(ScoreDoc doc : hits)
				doc.score = doc.score / maxScore;
			
			if(flag_verbose)
				System.out.println("\nFor query: " + stringQuery + "\n");
			
			/* reordenacao ponderada*/
			for(int i=0; i<hits.length; i++)
			{
				double docScore = alpha * hits[i].score;
				Document doc = searcher.doc(hits[i].doc);
				int ownerId = doc.getField("OwnerUserId").numericValue().intValue();
				double userScore = (1-alpha) * userRank.get(ownerId);
				
				calculatedScore[i] = docScore + userScore;
				
				if(flag_verbose)
					System.out.println("docscore: " + docScore + ", userScore: " + userScore + ", new score: " + calculatedScore[i]);
			}
			
			for(int i=0; i<10; i++)
			{
				double max = 0;
				int finalKey = -1;
				for(int j=0; j<calculatedScore.length; j++)
				{
					if(calculatedScore[j] == -1)
						continue;
					
					if(calculatedScore[j] > max)
					{
						max = calculatedScore[j]; 
						finalKey = j;
					}
				}
				if(finalKey == -1)
					System.out.println("ERRROR CALCULATINA SCORE");
				else
				{
					Document doc = searcher.doc(hits[finalKey].doc);
					resultList.add(new SearchResult(doc.getField("AnswerId").numericValue().intValue(), i+1, hits[finalKey].score));
					calculatedScore[finalKey] = -1;
				}
			}
				
			/* final da reordena��o ponderada*/
				
			/* BLIND reorder - based only on the user rank
			int numTotalHits = results.totalHits;
			System.out.println(numTotalHits + " total matching documents");
			
			Document[] NoRankTop = new Document[hits.length];
			
			// Order results by page rank
			for (int j = 0; j < hits.length; j++)
			{				
				Document doc = searcher.doc(hits[j].doc);
				NoRankTop[j] = doc;
			}
			
			for(int j = 0; j < 10; j++) {
				
				double max = 0;
				int maxId = 0;
				
				for(int k = 0; k < NoRankTop.length; k++) {
					
					if(NoRankTop[k] == null)
						continue;
					
					double rank = userRank.get(NoRankTop[k].getField("OwnerUserId").numericValue().intValue());
					if(rank > max) {
						max = rank;
						maxId = k;
					}
				}
				
				resultList.add(new SearchResult(NoRankTop[maxId].getField("AnswerId").numericValue().intValue(), j+1, hits[maxId].score));
				NoRankTop[maxId] = null;
			}*/
			
			//System.out.println("Explanation for document " + hits[0].doc + " over the query " + query.toString() + ":\n" + searcher.explain(query, hits[0].doc).toString());
			reader.close();
			return resultList;
		} 
		catch (IOException e) 
		{
			try 
			{
				reader.close();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
			e.printStackTrace();
			return null;
		}
	}

	void saveResultsTREC(ArrayList<QueryDoc> queries, String runId)
	{
		String SEPARATOR = "\t";
		String EOLINE = "\r\n";
		String q0 = "Q0";
		
		StringBuilder sb = new StringBuilder();
		sb.append("QueryId" + SEPARATOR);
		sb.append("QO" + SEPARATOR);
		sb.append("DocID" + SEPARATOR);
		sb.append("Rank" + SEPARATOR);
		sb.append("Score" + SEPARATOR);
		sb.append("RunID" + EOLINE);
		int i = 0;
		try
		{
			
			for(QueryDoc query : queries)
			{
				if(i==7)
					i--;
				for(SearchResult result : query.results)
				{
					sb.append(query.queryId + SEPARATOR);
					sb.append(q0 + SEPARATOR);
					sb.append(result.toString());
					sb.append(runId + EOLINE);
				}
				i++;
			}
		}
		catch (Exception e)
		{
			
		}
		try
		{
			PrintWriter writer = new PrintWriter(results, "UTF-8");
			writer.append(sb);
			writer.close();		
			System.out.println();
			System.out.println("RESULTS FILE CREATED");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	    
	    
	}
	
	void saveResultsKAGGLE(ArrayList<QueryDoc> queries, String runId)
	{
		String SEPARATOR = " ";
		String EOLINE = "\r\n";
		
		StringBuilder sb = new StringBuilder();
		sb.append("ID,");
		sb.append("AnswerID" + EOLINE);
		try
		{	
			for(QueryDoc query : queries)
			{
				sb.append(query.queryId + ",");
				for(SearchResult result : query.results)
					sb.append(result.answerId + SEPARATOR);
				sb.append(EOLINE);
			}
		}
		catch (Exception e)
		{
			
		}
		try
		{
			PrintWriter writer = new PrintWriter(results, "UTF-8");
			writer.append(sb);
			writer.close();		
			System.out.println();
			System.out.println("RESULTS FILE CREATED");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	    
	    
	}

	public void close() {
		try {
			idx.close();
		} catch (IOException e) {
			System.out.println("Error closing the index.");
		}
	}
	
	private static void printUsage() {
		
		System.out.println("Usage:\nFor command line: java [Mode] -cp bin:lib/* org.novasearch.tutorials.Lab0NovaBaseline [Options].");
		System.out.println("For Eclipse: Set [Mode] on JVM arguments, and set [Options] on Program Arguments.");
		System.out.print("\nMode:\nSet '-Ddev=true' on the JVM arguments field to enable dev mode (results constructed in TREC format using offline queries), ");
		System.out.println("or leave blank (results constructed in Kaggle format using kaggle queries).");
		System.out.println("\nOptions:\n-v, verbose mode.\n-e, evaluation mode (uses kaggle-public queries).\n");
	}
	
	private static HashMap<Integer, Double> loadUserRank(String file) {
		
		HashMap<Integer, Double> userRank = new HashMap<Integer, Double>();
		
		// File reader that reads user pageranks (format: UserID PageRank\n)
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			
			String line = br.readLine();
			String[] tokens;
			
			userRank = new HashMap<Integer, Double>();
			
			// ====================================================
			// Read user pageRank
			while (line != null) {
				tokens = line.split(" ");
				userRank.put(Integer.parseInt(tokens[0]), Double.parseDouble(tokens[1]));
				line = br.readLine();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userRank;
	}

	public static void main(String[] args) {

		printUsage();
		HashMap<Integer, Double> userRank = loadUserRank(pageRankPath);
		
		if(args.length > 0) {
			
			String argument = args[0];
			
			if(argument.startsWith("-")) {
				if(argument.contains("v"))
					flag_verbose = true;
				if(argument.contains("e"))
					flag_evaluation = true;
			}
		}
		
		if(DEV_MODE) {
			if(flag_evaluation) {
				queries = evaluationQueries;
				System.out.println("Using kaggle evaluation queries on TREC format");
			}
			else {
				queries = offlineQueries;
				System.out.println("Using offline queries on TREC format");
			}
		}
		
//		Analyzer analyzer = new StandardAnalyzer();
		Analyzer analyzer = new Lab1NovaAnalyser();
		
		Lab0NovaBaseline baseline = new Lab0NovaBaseline();
		baseline.openIndex(analyzer);
		baseline.indexDocuments();
		baseline.close();
		
		try {
			ArrayList<QueryDoc> queries = baseline.readQueries();

			for (QueryDoc query : queries)
				query.results = baseline.indexSearch(analyzer, query.text, userRank, 0.8);
			
			if(DEV_MODE)
				baseline.saveResultsTREC(queries, "run-1");
			else
				baseline.saveResultsKAGGLE(queries, "run-1");
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
