package org.novasearch.tutorials;

public class SearchResult {
	
	int answerId;
	int rank;
	float score;
		
	
	public SearchResult(int answerId, int rank, float score) {
		super();
		this.answerId = answerId;
		this.rank = rank;
		this.score = score;
	}
	public int getAnswerId() {
		return answerId;
	}
	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	
	@Override
	public String toString()
	{
		return answerId + "\t" + rank + "\t" + score + "\t";
		//return "AnswerId: " + getAnswerId() + "   Rank: " + getRank() + "     Score: " + getScore();
	}

}
