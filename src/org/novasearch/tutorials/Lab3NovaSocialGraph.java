package org.novasearch.tutorials;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;



public class Lab3NovaSocialGraph extends DatasetParser {

	public class User {
		public Integer Id;
		public double userRank, totalScore, totalAnswers;
		public List<Link> inLinks;
		public List<Link> outLinks;

		public User(Integer id) {
			Id = id;
			userRank = 0;
			totalScore = 0;
			totalAnswers = 0;
			inLinks = new ArrayList<>();
			outLinks = new ArrayList<>();
		}
		
		@Override
		public String toString()
		{
			return Id + " rank: " + userRank +  " inlinks: " + inLinks.size() + "   outLinks: " + outLinks.size(); 
		}
	};

	public class Link {
		public User srcUser;
		public User dstUser;
		public QA question;
		public QA answer;
	};

	// <QuestionId, Question>
	protected Map<Integer, QA> questionsMap;

	// <QuestionId, <ScoreId, Answer >>
	protected Map<Integer, TreeMap<Integer, QA>> answersMap;

	public Lab3NovaSocialGraph() {
		socialGraph = new HashMap<Integer, User>();

		questionsMap = new HashMap<Integer, QA>();
		answersMap = new HashMap<Integer, TreeMap<Integer, QA>>();
	}

	@Override
	protected void handleAnswer(QA tmp) {

		// Store the answer in the pool of
		// question-answers
		TreeMap<Integer, QA> answers = answersMap.get(tmp.parentId);
		if (answers == null)
			answers = new TreeMap<Integer, QA>();
		answers.put(tmp.Id, tmp);
		answersMap.put(tmp.parentId, answers);

	}

	@Override
	protected void handleQuestion(QA tmp) {
		questionsMap.put(tmp.Id, tmp);
	}

	protected HashMap<Integer, User> socialGraph;
	protected double damping;

	public void loadSocialGraph(String answersPath, String questionsPath, double d) {

		damping = d;

		loadData(questionsPath, answersPath);
		System.out.println("Answers size: " + answersMap.size());
		int counter = 0;
		for (Map.Entry<Integer, TreeMap<Integer, QA>> question : answersMap.entrySet()) {
			Integer QuestionId = question.getKey();
			TreeMap<Integer, QA> answers = question.getValue();
			QA q = questionsMap.get(QuestionId);
			if(q == null)
			{
				counter++;
				continue;
			}
			for (Map.Entry<Integer, QA> ans : answers.entrySet()) {
				QA a = ans.getValue();
				addLinkToGraph(q, a);
			}
			
		}
		System.out.println("Contador: " + counter);
	}

	public void addLinkToGraph(QA question, QA answer) {

		User questionUser;
		User answerUser;
		
		if(question.ownerUserId == null)
		{
			questionUser = socialGraph.get(0); //utilizador anonimo
			if(questionUser == null)
			{
				questionUser = new User(0);
				socialGraph.put(question.ownerUserId, questionUser);
			}			
		}
		else
		{
			questionUser = socialGraph.get(question.ownerUserId);
			if(questionUser == null)
			{
				questionUser = new User(question.ownerUserId);
				socialGraph.put(question.ownerUserId, questionUser);
			}			
		}
		
		if(answer.ownerUserId == null)
		{
			System.out.println("Anonymous answer!");
			answerUser = socialGraph.get(0); //utilizador anonimo
			if(answerUser == null)
			{
				answerUser = new User(0);
				socialGraph.put(answer.ownerUserId, answerUser);
			}
		}
		else
		{
			answerUser = socialGraph.get(answer.ownerUserId);
			if(answerUser == null) 
			{
				answerUser = new User(answer.ownerUserId);
				socialGraph.put(answer.ownerUserId, answerUser);
			}
		}
		
		answerUser.totalScore =+ answer.score;
		answerUser.totalAnswers++;
		
		//S� adiciona outlink e inlink se esse user ainda n�o existir na lista
		
		if(!ExistsLink(questionUser.outLinks, answerUser))
		{
			Link outlink = new Link();
			outlink.srcUser = questionUser;
			outlink.dstUser = answerUser;
			outlink.answer = answer;
			outlink.question = question;
			
			Link inlink = new Link();
			inlink.srcUser = questionUser;
			inlink.dstUser = answerUser;
			inlink.answer = answer;
			inlink.question = question;
			
			questionUser.outLinks.add(outlink);
			answerUser.inLinks.add(inlink);			
		}
	}
	
	private boolean ExistsLink(List<Link> list, User user)
	{
		for(Link link : list)
		{
			if(link.dstUser.Id.equals(user.Id))
				return true;
		}
		return false;
	}

	// iter - number of iterations, start with 3
	// in the end of each iteration, normalize PageRanks
	public void computePageRank(Integer iter) 
	{
		AssignInitialRank();
		int counter = 0;
		double d = 0.15;
		while(counter < iter)
		{
			System.out.println("iteration: " + counter);
			for(Entry<Integer, User> user_i : socialGraph.entrySet())
			{
				double rank = 0;
				for(Link link_from_j : user_i.getValue().inLinks)
					rank += (link_from_j.srcUser.userRank) * link_from_j.answer.score / link_from_j.srcUser.outLinks.size();
				user_i.getValue().userRank = (1-d) + (d*rank);
			}
			counter++;
		}
		
		// For priting, to visualize what is happening
		for(Entry<Integer, User> user_i : socialGraph.entrySet()) {
			User u = user_i.getValue();
			System.out.println("User " + u.Id + ":\n" + u.inLinks.size() + " inlink(s)\n" + u.outLinks.size() + " outlink(s)\nrank: " + u.userRank);
			if(!u.inLinks.isEmpty())
				System.out.println("--------- Links ---------");
			for(Link inlink : u.inLinks) {
				System.out.println("Link from " + inlink.srcUser.Id + " (rank " + inlink.srcUser.userRank + "), weight: " + inlink.answer.score);
			}
			System.out.println();
		}
		
		/*// Normalized, negative value avoiding way, while considering user_rank (not just score).
		double max = 0, min = 0;
		
		for(Entry<Integer, User> user_i : socialGraph.entrySet())
		{
			User u = user_i.getValue();
			double bonus = 0;
			
			if(u.totalAnswers != 0)
				bonus = u.totalScore/u.totalAnswers;
			
			if(bonus > max)
				max = bonus;
			else if(bonus < min)
				min = bonus;
		}
		
		// score average
		for(Entry<Integer, User> user_i : socialGraph.entrySet())
		{
			User u = user_i.getValue();
			
			if(u.totalAnswers != 0) {
				double bonus = u.totalScore/u.totalAnswers;
				System.out.print("User " + u.Id + " (" + u.inLinks.size() + " in, " + u.outLinks.size() + " out), rank: " + u.userRank);
				if(bonus > 0) {
					bonus = bonus/max;
					//exp = ("ID = " + u.Id + ": urank(" + u.userRank + ") * (1 - " + alpha + ") + urank(" + u.userRank + ") * " + bonus + " * " + alpha);
					u.userRank = u.userRank + u.userRank * bonus; //u.userRank + u.userRank * bonus;
					System.out.println(", avg_score: " + bonus + ", new_rank: " + u.userRank);
				} else if (bonus < 0){
					bonus = bonus/min;
					//exp = ("ID = " + u.Id + ": urank(" + u.userRank + ") * (1 - " + alpha + ") - urank(" + u.userRank + ") * " + bonus + " * " + alpha);
					u.userRank = u.userRank - u.userRank * bonus; //u.userRank - u.userRank * bonus;
					System.out.println(", avg_score: -" + bonus + ", new_rank: " + u.userRank);
				} else {
					System.out.println();
				}
			}
		}
		
		System.out.println("max bonus: " + max + ", min bonus: " + min);
		*/
		
		/*// From the min to the max, normalize like from 0 to 1 as a bonus
		double max = 0, min = 0;
		
		for(Entry<Integer, User> user_i : socialGraph.entrySet())
		{
			User u = user_i.getValue();
			
			double bonus = 0;
			
			if(u.totalAnswers > 0)
				bonus = u.totalScore/u.totalAnswers;
			if(bonus > max)
				max = bonus;
			else if(bonus < min)
				min = bonus;
		}
		
		// score average
		for(Entry<Integer, User> user_i : socialGraph.entrySet())
		{
			User u = user_i.getValue();
			
			if(u.totalAnswers != 0) {
				double bonus = u.totalScore/u.totalAnswers - min;
				System.out.println("Bonus + Min = " + bonus);
				bonus = bonus/(max-min);
				System.out.println("Non-negative bonus: " + bonus + ", (previousBonus/" + (max-min) + ")");
				System.out.print("User " + u.Id + " (" + u.inLinks.size() + " in, " + u.outLinks.size() + " out), rank: " + u.userRank);
				u.userRank = bonus;
				System.out.println(", avg_score: " + bonus + ", new_rank: " + u.userRank);
			}
		}*/
		
		/*// score average
		for(Entry<Integer, User> user_i : socialGraph.entrySet())
		{
			User u = user_i.getValue();
			
			if(u.totalAnswers != 0) {
				double bonus = u.totalScore/u.totalAnswers;
				System.out.print("User " + u.Id + " (" + u.inLinks.size() + " in, " + u.outLinks.size() + " out), rank: " + u.userRank);
				u.userRank = u.userRank + u.userRank * bonus;
				System.out.println(", avg_score: " + bonus + ", new_rank: " + u.userRank);
			}
		}*/
		
	}
	
	private void AssignInitialRank()
	{
		//System.out.println("ASSIGN INITIAL RANK");
		//System.out.println("");
		System.out.println("size: " + socialGraph.size());
		int size = socialGraph.size();
		for(Entry<Integer, User> user : socialGraph.entrySet())
		{
			user.getValue().userRank = 1.0 / size;
			//System.out.println(user.getValue().toString());
		}
		//System.out.println("END OF INITIAL RANK");
	}

	public void NormalizeRank()
	{
		double max = 0;
		for(Entry<Integer,User> user : socialGraph.entrySet())
			if(user.getValue().userRank > max)
				max = user.getValue().userRank;
		System.out.println("Top Rank: " + max);
		for(Entry<Integer,User> user : socialGraph.entrySet())
			user.getValue().userRank = user.getValue().userRank / max;
	}
	
	public void saveSocialGraph() throws FileNotFoundException, UnsupportedEncodingException
	{
		PrintWriter writer = new PrintWriter("data/pagerankSG.txt", "UTF-8");
		int size = socialGraph.size();
		writer.append("Number of users: " + size + "\r\n\r\n");
		for(Entry<Integer, User> user : socialGraph.entrySet())
		{
			writer.append(user.getKey() + " : " + user.getValue().toString() + "\r\n");
			//System.out.println("saving " + (counter++) + " of " + size);
		}
		writer.close();			
		System.out.println("social graph file created");
	}
	
	public void outUserRank() throws FileNotFoundException, UnsupportedEncodingException 
	{
		PrintWriter writer = new PrintWriter("data/pagerank.txt", "UTF-8");
		for (Entry<Integer, User> usr : socialGraph.entrySet()) 
		{
			writer.append(usr.getKey() + " " + usr.getValue().userRank + "\r\n");			
		}
		writer.close();
		System.out.println("pagerank.txt file created");
	}
	
	public void saveUsersIdList() throws FileNotFoundException, UnsupportedEncodingException
	{
		PrintWriter writer = new PrintWriter("data/users.txt", "UTF-8");
		for (Entry<Integer, User> usr : socialGraph.entrySet()) 
		{
			writer.append(usr.getKey() + "\r\n");			
		}
		writer.close();
		System.out.println("users.txt file created");
	}

	public static void main(String[] args) throws InterruptedException {

		Lab3NovaSocialGraph temp = new Lab3NovaSocialGraph();

		String answersPath = "/media/guilherme/STORAGE/Dropbox/Workspaces/eclipse_neon3_workspace/pesquisaweb/data/Answers.csv";
		String questionsPath = "/media/guilherme/STORAGE/Dropbox/Workspaces/eclipse_neon3_workspace/pesquisaweb/data/Questions.csv";
		

		temp.loadSocialGraph(answersPath, questionsPath, 0.1);
//		Iterator it = temp.socialGraph.entrySet().iterator();
//		while(it.hasNext())
//			{
//				String a = "";
//				Map.Entry pair = (Map.Entry)it.next();
//				int key = (int)pair.getKey();
//				User user = (User)pair.getValue();
//				System.out.println(user.toString());
//		        System.out.println(pair.getKey() + " : " + ((User)pair.getValue()).toString());
//			}
//			
		
		temp.computePageRank(10);
		try
		{
			temp.saveSocialGraph();
		}
		catch(Exception e)
		{
			System.out.println("Error saving social graph: " + e.getMessage());
		}
		temp.NormalizeRank();

		try
		{
			temp.outUserRank();
			temp.saveUsersIdList();
		}
		catch(Exception e)
		{
			System.out.println("Error saving page rank: " + e.getMessage());
		}

		return;

	}

}
