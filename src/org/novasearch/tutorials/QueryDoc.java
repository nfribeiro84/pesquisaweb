package org.novasearch.tutorials;

import java.util.ArrayList;

public class QueryDoc 
{
	int queryId;
	String text;
	ArrayList<SearchResult> results;
	
	public QueryDoc(int queryId, String text) {
		super();
		this.queryId = queryId;
		this.text = text;
		results = new ArrayList<SearchResult>();
	}

	@Override
	public String toString() {
		return queryId +":" + text ;
	}
	
	

}
